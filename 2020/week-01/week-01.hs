import Data.List(find)
import System.Environment(getArgs)

twoSumTo :: Int -> [Int] -> Maybe [Int]
twoSumTo _ [] = Nothing
twoSumTo desiredSum (x:xs) =
  case find (== desiredSum - x) xs of
    Nothing -> twoSumTo desiredSum xs
    Just y  -> Just [x, y]

threeSumTo :: Int -> [Int] -> Maybe [Int]
threeSumTo _ [] = Nothing
threeSumTo desiredSum (x:xs) =
  case twoSumTo remainder xs of
    Nothing -> threeSumTo desiredSum xs
    Just [y, z] -> Just [x, y, z]
  where remainder = desiredSum - x

main :: IO ()
main = do
  [desiredSumString, fileName] <- getArgs
  contents <- readFile fileName
  let numbers = map read . lines $ contents
  let desiredSum = read desiredSumString
  mapM_ print [twoSumTo desiredSum numbers, threeSumTo desiredSum numbers]
