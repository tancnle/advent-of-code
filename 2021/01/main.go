package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	input := readIntLines("input.txt")

	partOne(input)
	partTwo(input)
}

func partOne(input []int64) {
	var bump int64

	previous := input[0]
	for i := 1; i < len(input); i++ {
		current := input[i]
		if current > previous {
			bump++
		}
		previous = current
	}

	fmt.Println(bump)
}

func partTwo(input []int64) {
	var bump int64

	previous := input[0] + input[1] + input[2]
	for i := 1; i < len(input)-2; i++ {
		current := input[i] + input[i+1] + input[i+2]
		if current > previous {
			bump++
		}
		previous = current
	}

	fmt.Println(bump)

}

func readIntLines(file string) []int64 {
	f, _ := os.Open(file)
	defer f.Close()

	scanner := bufio.NewScanner(f)

	var input []int64

	for scanner.Scan() {
		value, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		input = append(input, value)
	}

	if scanner.Err() != nil {
		panic(scanner.Err())
	}

	return input
}
