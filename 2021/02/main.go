package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type instruction struct {
	movement string
	quantity int64
}

type position struct {
	x int64
	y int64
}

type tracking struct {
	position
	aim int64
}

func main() {
	input := readLines("input.txt")

	partOne(input)
	partTwo(input)
}

func partOne(input []*instruction) {
	var pos position

	for _, instruction := range input {
		switch instruction.movement {
		case "forward":
			pos.x = pos.x + instruction.quantity
		case "up":
			pos.y = pos.y - instruction.quantity
		case "down":
			pos.y = pos.y + instruction.quantity
		}
	}

	fmt.Println(pos.x * pos.y)
}

func partTwo(input []*instruction) {
	var track tracking

	for _, instruction := range input {
		switch instruction.movement {
		case "forward":
			track.x = track.x + instruction.quantity
			track.y = track.y + track.aim*instruction.quantity
		case "up":
			track.aim = track.aim - instruction.quantity
		case "down":
			track.aim = track.aim + instruction.quantity
		}
	}

	fmt.Println(track.x * track.y)
}

func readLines(file string) []*instruction {
	f, _ := os.Open(file)
	defer f.Close()

	scanner := bufio.NewScanner(f)

	var input []*instruction

	for scanner.Scan() {
		v := strings.Split(scanner.Text(), " ")
		quantity, _ := strconv.ParseInt(v[1], 10, 64)
		input = append(input, &instruction{movement: v[0], quantity: quantity})
	}

	if scanner.Err() != nil {
		panic(scanner.Err())
	}

	return input
}
