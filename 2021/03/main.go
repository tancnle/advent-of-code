package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type bits struct {
	ones  int64
	zeros int64
}

func main() {
	input := readLines("input.txt")

	partOne(input)
	partTwo(input)
}

func partOne(input []string) {
	var gamma int64
	var epsilon int64

	bins := count(input)

	for _, bin := range bins {
		gamma <<= 1
		epsilon <<= 1

		if bin.ones > bin.zeros {
			gamma |= 1
		} else {
			epsilon |= 1
		}
	}

	fmt.Println(gamma * epsilon)
}

func partTwo(input []string) {
	digits := len(input[0])

	filter := func(needle byte, input []string, index int) []string {
		var keeps []string

		for _, line := range input {
			if line[index] == needle {
				keeps = append(keeps, line)
			}
		}

		if len(keeps) != 0 {
			return keeps
		} else {
			return input
		}
	}

	// find oxygen
	var findOxygen func(input []string, index int) []string
	findOxygen = func(input []string, index int) []string {
		if len(input) == 1 || index > digits-1 {
			return input
		}

		bins := count(input)

		var mostCommonValues []string
		if bins[index].ones >= bins[index].zeros {
			mostCommonValues = filter('1', input, index)
		} else {
			mostCommonValues = filter('0', input, index)
		}

		return findOxygen(mostCommonValues, index+1)
	}

	oxygenLevel, _ := strconv.ParseInt(findOxygen(input, 0)[0], 2, 64)

	// find CO2
	var findCO2 func(input []string, index int) []string
	findCO2 = func(input []string, index int) []string {
		if len(input) == 1 || index > digits-1 {
			return input
		}

		bins := count(input)

		var leastCommonValues []string
		if bins[index].zeros <= bins[index].ones {
			leastCommonValues = filter('0', input, index)
		} else {
			leastCommonValues = filter('1', input, index)
		}

		return findCO2(leastCommonValues, index+1)
	}

	co2Level, _ := strconv.ParseInt(findCO2(input, 0)[0], 2, 64)

	fmt.Println(oxygenLevel * co2Level)
}

func count(input []string) []bits {
	bins := make([]bits, len(input[0]))

	for _, line := range input {
		for i, char := range line {
			if char == '0' {
				bins[i].zeros++
			} else {
				bins[i].ones++
			}
		}
	}

	return bins
}

func readLines(file string) []string {
	f, _ := os.Open(file)
	defer f.Close()

	scanner := bufio.NewScanner(f)

	var input []string

	for scanner.Scan() {
		input = append(input, scanner.Text())
	}

	if scanner.Err() != nil {
		panic(scanner.Err())
	}

	return input
}
